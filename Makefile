MAKE=make
SRCDIR=./src

_all: all
	
%:
	$(MAKE) -C $(SRCDIR) $@

