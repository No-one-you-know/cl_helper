#ifndef _CL_HELPER_H_
#define _CL_HELPER_H_

#include <CL/cl.h>
#include <stdio.h>
#include <stdlib.h>

void exitOnFailure(cl_int status, const char* message);

cl_uint getNumberPlatforms(void);
void getPlatformIDs(cl_platform_id * platformIDs, cl_uint numPlatforms);
cl_uint getNumberDevices(cl_platform_id platformID);
void getDeviceIDs(cl_platform_id platformID, cl_device_id * deviceIDs, cl_uint numDevices);

#endif /* _CL_HELPER_H_ */
