# OpenCL helper library
---

This library is meant to ease a bit the tedious and extense error handling
when programming with OpenCL. In the future this library may actually
abstract away a lot of OpenCL internals from your code.

### License
---

See LICENSE.md
