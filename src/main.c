#include <stdio.h>
#include <stdlib.h>
#include <cl_helper.h>

int main(int argc, char *argv[])
{
  int i;

  cl_platform_id platforms[5];
  cl_device_id devices[5][10];

  cl_uint numPlatforms, numDevices;

  numPlatforms = getNumberPlatforms();
  getPlatformIDs(platforms, numPlatforms);

  fprintf(stderr, "%u Platforms.\n", numPlatforms);

  for(i = 0; i < numPlatforms; i++){
    numDevices = getNumberDevices(platforms[i]);
    getDeviceIDs(platforms[i], devices[i], numDevices);

    fprintf(stderr, "%u Devices in platform %u.\n", numDevices, i+1);
  }
  
  return 0;
}
