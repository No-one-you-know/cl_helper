#include <cl_helper.h>

void exitOnFailure(cl_int status, const char* message){
  
  if( status != CL_SUCCESS ){
    fprintf(stderr, "Error: %s\n", message);
    fflush(stderr);
    exit(-1);
  }

  return;
}

cl_uint getNumberPlatforms(void){
  cl_int status;
  cl_uint numPlatforms = 0;

  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  exitOnFailure(status, "Unable to get number of available platforms.");

  return numPlatforms;
}

void getPlatformIDs(cl_platform_id * platformIDs, cl_uint numPlatforms){
  cl_int status;

  if(platformIDs == NULL){
    exitOnFailure(-1, "NULL pointer passed.");
  }

  status = clGetPlatformIDs(numPlatforms, platformIDs, NULL);
  exitOnFailure(status, "Unable to get list of opencl platforms.");

  return;
}

cl_uint getNumberDevices(cl_platform_id platformID){
  cl_int status;
  cl_uint numDevices;

  if(platformID == NULL){
    exitOnFailure(-1, "NULL pointer passed.");
  }

  status = clGetDeviceIDs(platformID, CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
  exitOnFailure(status, "Unable to get number of available devices.");

  return numDevices;
}

void getDeviceIDs(cl_platform_id platformID, cl_device_id * deviceIDs, cl_uint numDevices){
  cl_int status;

  if( deviceIDs == NULL){
    exitOnFailure(-1, "NULL pointer passed.");
  }

  status = clGetDeviceIDs(platformID, CL_DEVICE_TYPE_ALL, numDevices, deviceIDs, NULL);
  exitOnFailure(status, "Unable to get list of opencl devices.");

  return;
}


